<?php

abstract class Hewan extends Fight
{
    public $nama,
        $darah,
        $jumlahKaki,
        $keahlian;

    public function __construct($nama, $jumlahKaki, $keahlian = '', $attackPower, $defencePower, $darah = 50)
    {
        parent::__construct($attackPower, $defencePower);
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->darah = $darah;
    }

    function atraksi()
    {
        $string = "{$this->nama} sedang {$this->keahlian}";
        return $string;
    }

    abstract function getInfoHewan();
}

abstract class Fight
{
    public $attackPower,
        $defencePower;

    public function __construct($attackPower = 0, $defencePower = 0)
    {
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function serang($attacker, $defender)
    {
        $string = "{$attacker} menyerang {$defender}";
        return $string;
    }

    public function diserang($darah, $attackPowerAttacker, $defencePowerDefender)
    {
        $darahSetelahDiserang = $darah - $attackPowerAttacker / $defencePowerDefender;
        return $darahSetelahDiserang;
    }
}


class Harimau extends Hewan
{
    public function getInfoHewan()
    {
        $string = "Nama : {$this->nama} <br>
            Tipe: Harimau <br>
            Darah: {$this->darah} <br>
            Attack power: {$this->attackPower} <br>
            Defence power: {$this->defencePower} <br>
            Keahlian: {$this->keahlian}";

        return $string;
    }
}

class Elang extends Hewan
{
    public function getInfoHewan()
    {
        $string = "Nama : {$this->nama} <br>
            Tipe: Elang <br>
            Darah: {$this->darah} <br>
            Attack power: {$this->attackPower} <br>
            Defence power: {$this->defencePower} <br>
            Keahlian: {$this->keahlian}";

        return $string;
    }
}

$harimau1 = new Harimau('Harimau Alpha', 4, 'lari cepat', 7, 8);
$harimau2 = new Harimau('Harimau Charlie', 4, 'lari cepat', 7, 8);
$elang1 = new Elang('Elang Beta', 2, 'terbang tinggi', 10, 5);
echo '<br>';
echo $harimau1->getInfoHewan();
echo '<br>------------<br>';
echo $elang1->getInfoHewan();
echo '<br>------------<br>';
echo $harimau2->getInfoHewan();

echo '<h4>----- Fight Ronde 1  -----</h4>';
echo $elang1->serang($elang1->nama, $harimau1->nama);
$harimau1->darah = $harimau1->diserang($harimau1->darah, $elang1->attackPower, $harimau1->defencePower);
echo "<br>Sisa darah {$harimau1->nama} : {$harimau1->darah}";

echo '<h4>----- Fight Ronde 2  -----</h4>';
echo $harimau1->serang($harimau1->nama, $elang1->nama);
$elang1->darah = $elang1->diserang($elang1->darah, $harimau1->attackPower, $elang1->defencePower);
echo "<br>Sisa darah {$elang1->nama} : {$elang1->darah}<br>";
echo $harimau1->serang($harimau1->nama, $harimau2->nama);
$harimau2->darah = $harimau2->diserang($harimau2->darah, $harimau1->attackPower, $harimau2->defencePower);
echo "<br>Sisa darah {$harimau2->nama} : {$harimau2->darah}<br>";
